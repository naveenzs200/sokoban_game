#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <error.h>
#include <stdbool.h>

#define WALL '#'
#define PLAYER '@'
#define PLAYER_ON_GOAL '+'
#define BOX '$'
#define BOX_ON_GOAL '*'
#define FLOOR ' '
#define GOAL '.'

#define MOVE_UP 'u'
#define MOVE_DOWN 'd'
#define MOVE_LEFT 'l'
#define MOVE_RIGHT 'r'

#define FAILURE 0
#define SUCCESS 1
#define NO_BOX_MOVED 2
#define WON 3
#define NO_FURTHER_BOX_MOVES 4

int no_further_moves(char puzzle[][LINE_MAX], int box_row, int box_col)
{
        if (puzzle[box_row][box_col] == '$')
		if ((puzzle[box_row][box_col - 1] == '#' &&
		     puzzle[box_row - 1][box_col] == '#') ||

		    (puzzle[box_row][box_col + 1] == '#' &&
		     puzzle[box_row - 1][box_col] == '#') ||

		    (puzzle[box_row][box_col + 1] == '#' &&
		     puzzle[box_row + 1][box_col] == '#') ||

		    (puzzle[box_row][box_col - 1] == '#' &&
		     puzzle[box_row + 1][box_col] == '#'))
			return NO_FURTHER_BOX_MOVES;
		
	return FAILURE;
}

int all_boxes_in_place(char puzzle[][LINE_MAX])
{
	int row;
	int col;

	for (row = 0; row < 6; row++)
		for (col = 0; puzzle[row][col] != '\0'; col++)
			if (puzzle[row][col] == '$')
				return FAILURE;
	return SUCCESS;
}

int check_result(char puzzle[][LINE_MAX], int *box_row, int *box_col)
{
        if (*box_row == -1 && *box_col == -1)
		return NO_BOX_MOVED;

	if (no_further_moves(puzzle, *box_row, *box_col))
		return NO_FURTHER_BOX_MOVES;

	if (all_boxes_in_place(puzzle))
		return WON;

}

int get_user_input(char *user_in)
{
	char buff[LINE_MAX];
	char *ret;

	printf("Enter your move\n");
	while (1) {
		ret = fgets(buff, LINE_MAX, stdin);
		if (ret == NULL)
			error(1, 0, "Error reading from stdin\n");
		if (buff[1] != '\n'|| (buff[0] != MOVE_UP && buff[0] != MOVE_DOWN && buff[0] != MOVE_LEFT && buff[0] != MOVE_RIGHT)) {
			printf("Invalid move input.\nEnter again\n");
			continue;
		}
		break;
	}
	*user_in = buff[0];
	return 0; 
}
void update_position(int *r, int *c, char user_in)
{
	switch (user_in) {
	case MOVE_UP:
		(*r)--;
		break;
	case MOVE_DOWN:
		(*r)++;
		break;
	case MOVE_RIGHT:
		(*c)++;
		break;
	case MOVE_LEFT:
		(*c)--;
		break;
	}
 
}
int decide_move(char board_data[][LINE_MAX],int r, int c,char user_in, int box_pos[])
{
	int o_r = r;
	int o_c = c;
	switch(board_data[r][c]) {
	case FLOOR:
		board_data[r][c] = PLAYER;
		break;
	case GOAL:
		board_data[r][c] = PLAYER_ON_GOAL;
		break;
	case BOX:
		update_position(&r, &c, user_in);
		if(board_data[r][c] == ' ') {
			board_data[o_r][o_c] = PLAYER;
			board_data[r][c] = BOX;
			box_pos[0] = r;
			box_pos[1] = c;
		}
		else if(board_data[r][c] == '.') {
			board_data[o_r][o_c] = PLAYER;
			board_data[r][c] = BOX_ON_GOAL;
			box_pos[0] = r;
			box_pos[1] = c;
		}
		else
			return 1;;;
		break;
	case BOX_ON_GOAL:
		update_position(&r, &c, user_in);
		if(board_data[r][c] == ' ') {
			board_data[o_r][o_c] = PLAYER_ON_GOAL;
			board_data[r][c] = BOX;
			box_pos[0] = r;
			box_pos[1] = c;
		}
		else if(board_data[r][c] == '.') {
			board_data[o_r][o_c] = PLAYER_ON_GOAL;
			board_data[r][c] = BOX_ON_GOAL;
			box_pos[0] = r;
			box_pos[1] = c;
		}
		else
			return 1;
		break;
	case WALL:
		return 1;
		break;
	}
	return 0;
}
void update_move(char board_data[][LINE_MAX], char user_in,int box_pos[])
{
	int player_r, player_c;
	int r,c;
	int flag = 0;
	int i = 0;
	int j = 0;
	for (i = 0; i < 6 ; i++) {
		for(j = 0; j < LINE_MAX; j++) {
			if (board_data[i][j] == '@' || board_data[i][j] == '+') {
				player_r = i;
				player_c = j;
				break;
			}
		}
	}
	r = player_r;
	c = player_c;
	update_position(&r, &c, user_in);
	if (r < 0 || c < 0) {
		flag = 1;
	} else if (decide_move(board_data, r, c, user_in,box_pos) == 1) {
		flag = 1;
	} else {
		flag = 0;
	}

	if (flag != 1) {
		if (board_data[player_r][player_c] == PLAYER)
			board_data[player_r][player_c] = FLOOR;
		else
			board_data[player_r][player_c] = GOAL;
	} else
		printf("Invalid Move\n");
}
int main(void)
{
	char user_input;
	int box_position[2];
	int i;
	int rret;
	
	char board_data[][LINE_MAX] = {
		"########",
		"#     .#",
		"#@  $  #",
		"#   $  #",
		"# . *  #",
		"########",
		" "};
	for (i = 0; i < 6;i++ ) {
		printf("%s\n", board_data[i]);
	}
	while (1) {	
		box_position[0] = -1;
		box_position[1] = -1;
	        
		get_user_input(&user_input);
		update_move(board_data, user_input,box_position);

	        for (i = 0; i < 6;i++ ) {
			printf("%s\n", board_data[i]);
		}

		rret = check_result(board_data, &box_position[0], &box_position[1]);
		switch(rret) {
	        case NO_FURTHER_BOX_MOVES:
			printf("No further moves can be done to win\n");
			break;
		case NO_BOX_MOVED:
		        break;
		case WON:
			printf("Completed the level\n");
			break;
				
		}

	}
	return 0;
}
