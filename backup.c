
int loop_returns(void)
{
	return SUCCESS;
}

int no_further_moves(char **puzzle, int row, int col)
{
        if ((puzzle[row][col - 1] == '#' &&
	     puzzle[row - 1][col] == '#') ||

	    (puzzle[row][col + 1] == '#' &&
	     puzzle[row - 1][col] == '#') ||

	    (puzzle[row][col + 1] == '#' &&
	     puzzle[row + 1][col] == '#') ||

	    (puzzle[row][col - 1] == '#' &&
	     puzzle[row + 1][col] == '#'))
		return FAILURE;

	return SUCCESS;
}

int validate_puzzle(char **puzzle, int rlen)
{
	int i;
	int j;
	int ret;
	int count = 0;

	for (i = 0; i < rlen; i++)
		for (j = 0; j < strlen(puzzle[i]); j++) {
			if (puzzle[i][j] == '$') {
				ret = no_further_moves(puzzle, i, j);
				if (ret)
					return FAILURE;
				count++;
			} else if (puzzle[i][j] == '.') {
				count--;
			}
		}	
	if (count == 0)
		return SUCCESS;
	return FAILURE;
}

int read_puzzle(char **puzzle, FILE *fp)
{
	int i = 0;
	char *retp;
	int flag = 0;
	int count = 0;
	char buf[LINE_MAX];
	char level[LINE_MAX];
	char str[LINE_MAX];
	int ret;

	while (1) {
		retp = fgets(str, sizeof(str), fp);
		if (retp == NULL) {
			break;
		} else if (str[0] == ';') {
			strcpy(level, str + 2);
			printf("%s", level);
			fgets(buf, sizeof(buf), fp);
			break;
		}
		puzzle[i] = (char *)malloc(sizeof(char));
		strcpy(puzzle[i], str);
		i++;
	}
	return i;
}

int main(void)
{
	int j;
	int  i = 0;
	FILE *fp;
	int rlen;
	int ret;
	char user_input;
	int box_position[2];
	int rret;
	char *puzzle[LINE_MAX]; /* Contains puzzle corresponding to level*/

	fp = fopen("test.txt", "r");
	if (fp == NULL)
		error(1, errno, "Error in opening the file");

	for (i = 0; rret == WON; i++) {
		rlen = read_puzzle(puzzle, fp);
		if (rlen == EOF)
			break;
		ret = validate_puzzle(puzzle, rlen);
		if (ret) {
			printf("Invalid puzzle\n");
			exit(FAILURE);
		} else {
			while (1) {
				box_position[0] = -1;
				box_position[1] = -1;
	        
				get_user_input(&user_input);
				update_move(puzzle, user_input,box_position);

				for (i = 0; i < 6;i++ ) {
					printf("%s\n", puzzle[i]);
				}
				rret = check_result(puzzle, &box_position[0], &box_position[1]);
				if (rret == WON)
					break;
			}
		}
	}

	printf("rlen %d\n", rlen);
		
	for (j = 0; j < rlen; j++)
		printf("%s", puzzle[j]);

	ret = fclose(fp);
	if (ret == -1)
		error(1, errno, "error in closing the file");
	return SUCCESS;
}
