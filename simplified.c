#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <error.h>
#include <limits.h>
#include <errno.h>

#define SUCCESS 0
#define FAILURE 1

void display(char **puzzle, int rows)
{
	int i = 0;

	for (i = 0; i < rlen;i++ )
		printf("%s", puzzle[i]);
}
	
void back_up_puzzle(char **puzzle, int rows, char **buf)
{
	int i;

	for(i = 0; i < rows; i++)
		buf[i] = puzzle[i];
}

int validate_puzzle(char **puzzle, int rlen)
{
	int i;
	int j;
	int ret;
	int count = 0;

	for (i = 0; i < rlen; i++)
		for (j = 0; j < strlen(puzzle[i]); j++) {
			if (puzzle[i][j] == '$') {
				ret = no_further_moves(puzzle, i, j);
				if (ret)
					return FAILURE;
				count++;
			} else if (puzzle[i][j] == '.') {
				count--;
			}
		}	
	if (count == 0)
		return SUCCESS;
	return FAILURE;
}

int read_puzzle(char **puzzle, FILE *fp)
{
	int i = 0;
	char *retp;
	int flag = 0;
	int count = 0;
	char buf[LINE_MAX];
	char level[LINE_MAX];
	char str[LINE_MAX];
	int ret;

	while (1) {
		retp = fgets(str, sizeof(str), fp);
		if (retp == NULL) {
			break;
		} else if (str[0] == ';') {
			strcpy(level, str + 2);
			printf("%s", level);
			fgets(buf, sizeof(buf), fp);
			break;
		}
		puzzle[i] = (char *)malloc(sizeof(char));
		strcpy(puzzle[i], str);
		i++;
	}
	return i;
}

FILE *open_file(char *str)
{
	return fopen(str, "r");
}

int main(void)
{
	int rows;
	int ret;
	FILE *fp;
	char *buf[LINE_MAX];
	char *puzzle[LINE_MAX]; /* Contains puzzle corresponding to level*/	

	fp = open_file("test.txt")
	if (fp == NULL)
		error(1, errno, "Error in opening the file");

	while (1) {
		rows = read_puzzle(puzzle, fp);
		if (rlen == EOF)
			break;

		ret = validate_puzzle(puzzle, rows);
		if (ret) {
			printf("Invalid Puzzle\n");
			exit(FAILURE);
		}

		back_up_puzzle(puzzle, rows, buf);
		display(puzzle, rows);
	}

	ret = fclose(fp);
	if (ret == -1)
		error(1, errno, "error in closing the file");
	return SUCCESS;
}
