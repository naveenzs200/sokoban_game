#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <error.h>
#include <limits.h>
#include <errno.h>

#define NULL_ERR 1
#define SUCCESS 0
#define FAILURE 1

#define WALL '#'
#define PLAYER '@'
#define PLAYER_ON_GOAL '+'
#define BOX '$'
#define BOX_ON_GOAL '*'
#define FLOOR ' '
#define GOAL '.'

#define MOVE_UP 'u'
#define MOVE_DOWN 'd'
#define MOVE_LEFT 'l'
#define MOVE_RIGHT 'r'

#define FAILURE_F 0
#define SUCCESS_S 1
#define NO_BOX_MOVED 2
#define WON 3
#define NO_FURTHER_BOX_MOVES 4


int no_further_moves_to_play(char **puzzle, int box_row, int box_col)
{
        if (puzzle[box_row][box_col] == '$')
		if ((puzzle[box_row][box_col - 1] == '#' &&
		     puzzle[box_row - 1][box_col] == '#') ||

		    (puzzle[box_row][box_col + 1] == '#' &&
		     puzzle[box_row - 1][box_col] == '#') ||

		    (puzzle[box_row][box_col + 1] == '#' &&
		     puzzle[box_row + 1][box_col] == '#') ||

		    (puzzle[box_row][box_col - 1] == '#' &&
		     puzzle[box_row + 1][box_col] == '#'))
			return NO_FURTHER_BOX_MOVES;
		
	return FAILURE_F;
}

int all_boxes_in_place(char **puzzle)
{
	int row;
	int col;

	for (row = 0; row < 6; row++)
		for (col = 0; puzzle[row][col] != '\0'; col++)
			if (puzzle[row][col] == '$')
				return FAILURE_F;
	return SUCCESS_S;
}

int check_result(char **puzzle, int *box_row, int *box_col)
{
        if (*box_row == -1 && *box_col == -1)
		return NO_BOX_MOVED;

	if (no_further_moves_to_play(puzzle, *box_row, *box_col))
		return NO_FURTHER_BOX_MOVES;

	if (all_boxes_in_place(puzzle))
		return WON;

}

int get_user_input(char *user_in)
{
	char buff[LINE_MAX];
	char *ret;

	printf("Enter your move\n");
	while (1) {
		ret = fgets(buff, LINE_MAX, stdin);
		if (ret == NULL)
			error(1, 0, "Error reading from stdin\n");
		if (buff[1] != '\n'|| (buff[0] != MOVE_UP && buff[0] != MOVE_DOWN && buff[0] != MOVE_LEFT && buff[0] != MOVE_RIGHT)) {
			printf("Invalid move input.\nEnter again\n");
			continue;
		}
		break;
	}
	*user_in = buff[0];
	return 0; 
}
void update_position(int *r, int *c, char user_in)
{
	switch (user_in) {
	case MOVE_UP:
		(*r)--;
		break;
	case MOVE_DOWN:
		(*r)++;
		break;
	case MOVE_RIGHT:
		(*c)++;
		break;
	case MOVE_LEFT:
		(*c)--;
		break;
	}
 
}
int decide_move(char **board_data,int r, int c,char user_in, int box_pos[])
{
	int o_r = r;
	int o_c = c;
	switch(board_data[r][c]) {
	case FLOOR:
		board_data[r][c] = PLAYER;
		break;
	case GOAL:
		board_data[r][c] = PLAYER_ON_GOAL;
		break;
	case BOX:
		update_position(&r, &c, user_in);
		if(board_data[r][c] == ' ') {
			board_data[o_r][o_c] = PLAYER;
			board_data[r][c] = BOX;
			box_pos[0] = r;
			box_pos[1] = c;
		}
		else if(board_data[r][c] == '.') {
			board_data[o_r][o_c] = PLAYER;
			board_data[r][c] = BOX_ON_GOAL;
			box_pos[0] = r;
			box_pos[1] = c;
		}
		else
			return 1;;;
		break;
	case BOX_ON_GOAL:
		update_position(&r, &c, user_in);
		if(board_data[r][c] == ' ') {
			board_data[o_r][o_c] = PLAYER_ON_GOAL;
			board_data[r][c] = BOX;
			box_pos[0] = r;
			box_pos[1] = c;
		}
		else if(board_data[r][c] == '.') {
			board_data[o_r][o_c] = PLAYER_ON_GOAL;
			board_data[r][c] = BOX_ON_GOAL;
			box_pos[0] = r;
			box_pos[1] = c;
		}
		else
			return 1;
		break;
	case WALL:
		return 1;
		break;
	}
	return 0;
}
void update_move(char **board_data, char user_in,int box_pos[])
{
	int player_r, player_c;
	int r,c;
	int flag = 0;
	int i = 0;
	int j = 0;
	for (i = 0; i < 6 ; i++) {
		for(j = 0; j < LINE_MAX; j++) {
			if (board_data[i][j] == '@' || board_data[i][j] == '+') {
				player_r = i;
				player_c = j;
				break;
			}
		}
	}
	r = player_r;
	c = player_c;
	update_position(&r, &c, user_in);
	if (r < 0 || c < 0) {
		flag = 1;
	} else if (decide_move(board_data, r, c, user_in,box_pos) == 1) {
		flag = 1;
	} else {
		flag = 0;
	}

	if (flag != 1) {
		if (board_data[player_r][player_c] == PLAYER)
			board_data[player_r][player_c] = FLOOR;
		else
			board_data[player_r][player_c] = GOAL;
	} else
		printf("Invalid Move\n");
}

int no_further_moves(char **puzzle, int row, int col)
{
        if ((puzzle[row][col - 1] == '#' &&
	     puzzle[row - 1][col] == '#') ||

	    (puzzle[row][col + 1] == '#' &&
	     puzzle[row - 1][col] == '#') ||

	    (puzzle[row][col + 1] == '#' &&
	     puzzle[row + 1][col] == '#') ||

	    (puzzle[row][col - 1] == '#' &&
	     puzzle[row + 1][col] == '#'))
		return FAILURE;

	return SUCCESS;
}

int validate_puzzle(char **puzzle, int rlen)
{
	int i;
	int j;
	int ret;
	int count = 0;

	for (i = 0; i < rlen; i++)
		for (j = 0; j < strlen(puzzle[i]); j++) {
			if (puzzle[i][j] == '$') {
				ret = no_further_moves(puzzle, i, j);
				if (ret)
					return FAILURE;
				count++;
			} else if (puzzle[i][j] == '.') {
				count--;
			}
		}	
	if (count == 0)
		return SUCCESS;
	return FAILURE;
}

int read_puzzle(char **puzzle, FILE *fp)
{
	int i = 0;
	char *retp;
	int flag = 0;
	int count = 0;
	char buf[LINE_MAX];
	char level[LINE_MAX];
	char str[LINE_MAX];
	int ret;

	while (1) {
		retp = fgets(str, sizeof(str), fp);
		if (retp == NULL) {
			break;
		} else if (str[0] == ';') {
			strcpy(level, str + 2);
			printf("%s", level);
			fgets(buf, sizeof(buf), fp);
			break;
		}
		puzzle[i] = (char *)malloc(sizeof(char));
		strcpy(puzzle[i], str);
		i++;
	}
	return i;
}

int main(void)
{
	int j;
	int  i = 0;
	FILE *fp;
	int rlen;
	int ret;
	char user_input;
	int box_position[2];
	int rret;
	char *puzzle[LINE_MAX]; /* Contains puzzle corresponding to level*/

	fp = fopen("test.txt", "r");
	if (fp == NULL)
		error(1, errno, "Error in opening the file");

	for (i = 0; ; i++) {
		rlen = read_puzzle(puzzle, fp);
		if (rlen == EOF)
			break;
		ret = validate_puzzle(puzzle, rlen);
		if (ret) {
			printf("Invalid puzzle\n");
			exit(FAILURE);
		} else {
			for (j = 0; j < rlen; j++)
				printf("%s", puzzle[j]);
			while (1) {
				box_position[0] = -1;
				box_position[1] = -1;
	        
				get_user_input(&user_input);
				update_move(puzzle, user_input,box_position);

				for (i = 0; i < rlen;i++ ) {
					printf("%s", puzzle[i]);
				}
				rret = check_result(puzzle, &box_position[0], &box_position[1]);
				if (rret == WON)
					break;
				else if (rret == NO_FURTHER_BOX_MOVES)
					exit(FAILURE);
			}
		}
	}

	ret = fclose(fp);
	if (ret == -1)
		error(1, errno, "error in closing the file");
	return SUCCESS;
}

